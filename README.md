# About #

This is a view that I quickly put together to resemble the Reminders app in iOS 8/9. I used it mainly for images. Will try to update and add new features to it. Hope it helps someone out there.

# Installation #

Add the **CDMVerticalCarousel** folder that contains **CDMVerticalCarousel.m/.h** and **CDMVerticalCarouselView.m/.h** files to your project.

# Usage #

It's designed to work like a UITableView. It has a delegate and data source. Download the example project and make tweaks to it.