//
//  VerticalCarouselView.m
//  CDMVerticalCarousel
//
//  Created by Claudiu Miron on 18/12/15.
//  Copyright © 2015 Claudiu Miron. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to
//  deal in the Software without restriction, including without limitation the
//  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
//  sell copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
//  IN THE SOFTWARE.

#import "VerticalCarouselView.h"

@implementation VerticalCarouselView

- (UILabel *)label
{
    if (!_label)
    {
        UILabel *theLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        theLabel.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self addSubview:theLabel];
        
        NSLayoutConstraint *centerX =
        [NSLayoutConstraint constraintWithItem:theLabel
                                     attribute:NSLayoutAttributeCenterX
                                     relatedBy:NSLayoutRelationEqual
                                        toItem:self
                                     attribute:NSLayoutAttributeCenterX
                                    multiplier:1
                                      constant:0];
        [self addConstraint:centerX];
        
        NSLayoutConstraint *top =
        [NSLayoutConstraint constraintWithItem:theLabel
                                     attribute:NSLayoutAttributeTop
                                     relatedBy:NSLayoutRelationEqual
                                        toItem:self
                                     attribute:NSLayoutAttributeTop
                                    multiplier:1
                                      constant:10];
        [self addConstraint:top];
        
        _label = theLabel;
    }
    
    return _label;
}

- (void)setSelected:(BOOL)selected
{
    [super setSelected:selected];
    if (selected)
    {
        self.label.textColor = [UIColor redColor];
    }
    else
    {
        self.label.textColor = [UIColor blackColor];
    }
}

@end
