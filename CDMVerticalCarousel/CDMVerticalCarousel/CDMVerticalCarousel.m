//
//  CDMVerticalCarousel.m
//  CDMVerticalCarousel
//
//  Created by Claudiu Miron on 15/12/15.
//  Copyright © 2015 Claudiu Miron. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to
//  deal in the Software without restriction, including without limitation the
//  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
//  sell copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
//  IN THE SOFTWARE.

#import "CDMVerticalCarousel.h"

@interface CDMItemRecord : NSObject

@property CGFloat  itemY;
@property CGFloat  itemWidth;

@property CDMVerticalCarouselView  *cachedView;

@end

@implementation CDMItemRecord

@end


@interface CDMVerticalCarousel ()

@property (nonatomic) NSMutableSet       *queuedItems;
@property (nonatomic) NSMutableIndexSet  *visibleItems;

@property NSMutableArray                 *itemRecords;

@property float shadowAlpha;

@property BOOL isShowingSelected;

@end

@implementation CDMVerticalCarousel

@dynamic delegate;

#pragma mark - Initialisation

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        [self setup];
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self)
    {
        [self setup];
    }
    
    return self;
}

#pragma mark - Public methods

- (CDMVerticalCarouselView *)viewForItemAtIndex:(NSUInteger)index
{
    CDMVerticalCarouselView *view = [self cachedViewForItem:index];
    
    if (!view)
    {
        [self.dataSource verticalCarousel:self viewForIndex:index];
    }
    
    return view;
}

- (CDMVerticalCarouselView *)dequeueReusableView
{
    CDMVerticalCarouselView *queueView = self.queuedItems.anyObject;
    
    if (queueView)
    {
        [self.queuedItems removeObject:queueView];
    }
    
    return queueView;
}

- (void) reloadData
{
    [self returnNonVisibleItemsToTheQueue:nil];
    [self generateHeightAndOffsetData];
    [self setNeedsLayout];
    [self layoutIfNeeded];
}

#pragma mark - Helpers and lazy loading

- (void)setup
{
    self.itemSpacing                            = 60.0;
    self.heightProportion                       = 1;
    self.paddingInsets                          = UIEdgeInsetsZero;
    self.falsePerspectiveMinimumWidthProportion = 1.0;
    self.shadowAlpha = 0.15;
    self.showsHorizontalScrollIndicator = NO;
    self.showsVerticalScrollIndicator = NO;
    self.delaysContentTouches = YES;
    self.alwaysBounceVertical = YES;
    self.alwaysBounceHorizontal = NO;
}

- (void)setItemShadows:(BOOL)itemShadows
{
    _itemShadows = itemShadows;
    
    [self makeShadows];
}

- (CGFloat)itemYForItem:(NSUInteger)item
{
    CGFloat theY = ((CDMItemRecord *)[self.itemRecords objectAtIndex:item]).itemY;
    
    return theY;
}

- (NSMutableSet*)queuedItems;
{
    if (!_queuedItems)
    {
        
        _queuedItems = [[NSMutableSet alloc] init];
    }
    
    return _queuedItems;
}

- (NSMutableIndexSet*)visibleItems;
{
    if (!_visibleItems)
    {
        _visibleItems = [[NSMutableIndexSet alloc] init];
    }
    
    return _visibleItems;
}

- (void)makeShadows
{
    if (self.visibleItems.count == 0) return;
    
    float maxAlpha = self.shadowAlpha;
    
    [self.visibleItems enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL * _Nonnull stop) {
        CDMVerticalCarouselView *view = [self cachedViewForItem:idx];
        
        [self applyShadowWithRadius:8
                             colour:[UIColor colorWithRed:0
                                                    green:17.0/255.0
                                                     blue:60.0/255.0
                                                    alpha:1.0]
                              alpha:maxAlpha
                          andOffset:CGSizeMake(0, -10)
                             onView:view];
    }];
    
    if (self.visibleItems.count == 1) return;
    
    NSUInteger secondItem =
    [self.visibleItems indexPassingTest:^BOOL(NSUInteger idx, BOOL * _Nonnull stop) {
        NSUInteger firstIndex = self.visibleItems.firstIndex;
        
        if(idx - firstIndex == 1) {
            *stop = YES;
            return YES;
        }
        
        return NO;
    }];
    
    CDMVerticalCarouselView *secondView = [self cachedViewForItem:secondItem];
    
    float alpha = (secondView.frame.origin.y - self.contentOffset.y - self.paddingInsets.top) / self.itemSpacing;
    
    if (alpha > 1) alpha = 1;
    
    [self applyShadowWithRadius:8
                         colour:[UIColor colorWithRed:0
                                                green:17.0/255.0
                                                 blue:60.0/255.0
                                                alpha:1]
                          alpha:alpha * maxAlpha
                      andOffset:CGSizeMake(0, -10)
                         onView:secondView];
}

- (void)applyShadowWithRadius:(CGFloat)radius
                       colour:(UIColor *)colour
                        alpha:(CGFloat)alpha
                    andOffset:(CGSize)offset
                       onView:(CDMVerticalCarouselView *)view
{
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:view.bounds];
    
    view.layer.masksToBounds = NO;
    view.layer.shadowColor   = colour.CGColor;
    view.layer.shadowRadius  = radius;
    view.layer.shadowOffset  = offset;
    view.layer.shadowOpacity = alpha;
    view.layer.shadowPath    = shadowPath.CGPath;
}

#pragma mark - Logic runners

- (void)returnNonVisibleItemsToTheQueue:(NSMutableIndexSet *)currentVisibleItems
{
    [self.visibleItems removeIndexes:currentVisibleItems];
    
    [self.visibleItems enumerateIndexesUsingBlock:^(NSUInteger item, BOOL *stop)
     {
         CDMVerticalCarouselView *view = [self cachedViewForItem:item];
         
         if (view)
         {
             [self.queuedItems addObject:view];
             [view removeFromSuperview];
             [self setCachedView:nil forItem:item];
         }
     }];
    
    self.visibleItems = currentVisibleItems;
}

- (void)setCachedView:(CDMVerticalCarouselView *)view forItem:(NSUInteger)item
{
    ((CDMItemRecord *) [self.itemRecords objectAtIndex:item]).cachedView = view;
}

- (CDMVerticalCarouselView *)cachedViewForItem:(NSUInteger)item
{
    return ((CDMItemRecord *)[self.itemRecords objectAtIndex:item]).cachedView;
}

#pragma mark - Layout methods

- (void)setPaddingInsets:(UIEdgeInsets)paddingInsets
{
    _paddingInsets = paddingInsets;
    
    [self generateHeightAndOffsetData];
}

- (void)generateHeightAndOffsetData
{
    CGFloat currentOffsetY = 0;
    
    NSMutableArray *newItemRecords = [[NSMutableArray alloc] init];
    
    NSUInteger numberOfItems = [self.dataSource numberOfItemsForVerticalCarousel:self];
    
    for (NSUInteger item = 0; item < numberOfItems; item++)
    {
        CDMItemRecord *itemRecord = [[CDMItemRecord alloc] init];
        
        itemRecord.itemY = self.itemSpacing * item;
        
        [newItemRecords insertObject:itemRecord atIndex:item];
    }
    
    currentOffsetY += (numberOfItems - 1 ) * self.itemSpacing;
    
    currentOffsetY += self.heightProportion * (self.frame.size.width - self.paddingInsets.left - self.paddingInsets.right);
    
    currentOffsetY += self.paddingInsets.top;
    
    currentOffsetY += self.paddingInsets.bottom;
    
    self.itemRecords = newItemRecords;
    
    self.contentSize = CGSizeMake(0, currentOffsetY);
}

- (NSInteger)findItemForOffsetY:(CGFloat)yPosition inRange:(NSRange)range
{
    if (self.itemRecords.count == 0) return 0;
    
    CDMItemRecord *itemRecord = [[CDMItemRecord alloc] init];
    itemRecord.itemY = yPosition;
    
    NSInteger returnValue = [self.itemRecords indexOfObject:itemRecord
                                              inSortedRange:range
                                                    options:NSBinarySearchingInsertionIndex
                                            usingComparator:^NSComparisonResult(CDMItemRecord *itemRecord1, CDMItemRecord *itemRecord2)
                             {
                                 if (itemRecord1.itemY < itemRecord2.itemY)
                                     return NSOrderedAscending;
                                 return NSOrderedDescending;
                             }];
    
    if(returnValue == 0) return 0;
    
    return returnValue - 1;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    static CGRect CDMVCOldFrame;
    
    if (!CGRectEqualToRect(CDMVCOldFrame, self.frame))
    {
        CDMVCOldFrame = self.frame;
        
        [self returnNonVisibleItemsToTheQueue:nil];
        [self generateHeightAndOffsetData];
    }
    
    CGFloat currentStartY = self.contentOffset.y;
    CGFloat currentEndY = currentStartY + self.frame.size.height;
    
    NSUInteger itemToDisplay =
    [self findItemForOffsetY:currentStartY
                     inRange:NSMakeRange(0, self.itemRecords.count)];
    
    NSMutableIndexSet *newVisibleItems = [[NSMutableIndexSet alloc] init];
    
    CGFloat yOrigin;
    
    while(yOrigin + self.paddingInsets.top + self.itemSpacing - /*shadow*/ 20 < currentEndY && itemToDisplay < self.itemRecords.count)
    {
        [newVisibleItems addIndex:itemToDisplay];
        
        yOrigin = [self itemYForItem:itemToDisplay];
        
        CDMVerticalCarouselView *view = [self cachedViewForItem:itemToDisplay];
        
        if (!view)
        {
            view = [self.dataSource verticalCarousel:self
                                        viewForIndex:itemToDisplay];
            
            [self setCachedView:view forItem:itemToDisplay];
            [self addSubview:view];
        }
        
        CGRect viewFrame = view.frame;
        
        CGFloat totalWidth = self.frame.size.width - self.paddingInsets.left - self.paddingInsets.right;
        
        if (yOrigin < self.contentOffset.y)
        {
            viewFrame.origin.y = self.contentOffset.y + self.paddingInsets.top;
        }
        else if (self.contentOffset.y < 0)
        {
            viewFrame.origin.y = yOrigin + self.paddingInsets.top - itemToDisplay * itemToDisplay * self.contentOffset.y * 0.03;
        }
        else
        {
            viewFrame.origin.y = yOrigin + self.paddingInsets.top;
        }
        
        CGFloat proportion = (viewFrame.origin.y - self.contentOffset.y - self.paddingInsets.top) / (self.itemSpacing * 2);
        
        if (proportion < 0)
        {
            proportion = 0;
        }
        else if (proportion > 1)
        {
            proportion = 1;
        }
        
        CGFloat minWidth = totalWidth * self.falsePerspectiveMinimumWidthProportion;
        CGFloat difference = totalWidth - (totalWidth * self.falsePerspectiveMinimumWidthProportion);
        
        viewFrame.size.width = minWidth + difference * proportion;
        viewFrame.origin.x = (totalWidth - viewFrame.size.width) / 2 + self.paddingInsets.left;
        viewFrame.size.height = viewFrame.size.width * self.heightProportion;
        
        view.translatesAutoresizingMaskIntoConstraints = YES;
        
        [self bringSubviewToFront:view];
        
        view.frame = viewFrame;
        
        if (view.gestureRecognizers.count == 0)
        {
            UITapGestureRecognizer *tap =
            [[UITapGestureRecognizer alloc] initWithTarget:self
                                                    action:@selector(tappedOnItem:)];
            
            tap.numberOfTapsRequired = 1;
            tap.numberOfTouchesRequired = 1;
            [view addGestureRecognizer:tap];
        }
        
        itemToDisplay++;
    }
    
    [self returnNonVisibleItemsToTheQueue:newVisibleItems];
    
    if(self.itemShadows) [self makeShadows];
}

#pragma mark - Touches

- (void)tappedOnItem:(UITapGestureRecognizer *)tap
{
    CDMVerticalCarouselView *tappedView = (CDMVerticalCarouselView *) tap.view;
    
    static NSArray *CDMVCInitialFrames;
    
    // This prevents more than one view being selected
    if (!self.scrollEnabled && !tappedView.selected) return;
    
    NSUInteger index =
    [self.visibleItems indexPassingTest:^BOOL(NSUInteger idx, BOOL * _Nonnull stop)
     {
         if ([[self cachedViewForItem:idx] isEqual:tappedView])
         {
             *stop = YES;
             return YES;
         }
         
         return NO;
     }];
    
    CGFloat fullWidth =
    self.bounds.size.width - self.paddingInsets.left -
    self.paddingInsets.right;
    
    CGRect bigFrame =
    CGRectMake(self.paddingInsets.left,
               self.paddingInsets.top + self.contentOffset.y,
               fullWidth,
               self.heightProportion * fullWidth);
    
    self.scrollEnabled = NO;
    
    if(self.itemShadows)
    {
        CGRect shadowFrame = CGRectMake(0,
                                        0,
                                        fullWidth,
                                        fullWidth * self.heightProportion);
        
        [self animateShadow:tappedView
               withMaxAlpha:self.shadowAlpha
                  andBounds:shadowFrame];
    }
    
    if (tappedView.selected)
    {
        [self.visibleItems enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL * _Nonnull stop) {
            CDMVerticalCarouselView *view = [self cachedViewForItem:idx];
            
            NSUInteger frameIndex = idx - self.visibleItems.firstIndex;
            
            CGRect initialFrame = [CDMVCInitialFrames[frameIndex] CGRectValue];
            
            [UIView animateWithDuration:0.6
                                  delay:0
                 usingSpringWithDamping:1
                  initialSpringVelocity:0
                                options:UIViewAnimationOptionLayoutSubviews
                             animations:^{
                                 view.frame = initialFrame;
                             }
                             completion:^(BOOL finished) {
                                 self.scrollEnabled = YES;
                             }];
        }];
        
        CDMVCInitialFrames = nil;
        
        [self.delegate verticalCarousel:self didDeselectItemAtIndex:index];
    }
    else
    {
        NSMutableArray *temp =
        [[NSMutableArray alloc] initWithCapacity:self.visibleItems.count];
        
        [self.visibleItems enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL * _Nonnull stop) {
            CDMVerticalCarouselView *view = [self cachedViewForItem:idx];
            
            [temp addObject:[NSValue valueWithCGRect:view.frame]];
            
            CGRect frame = view.frame;
            frame.origin.y = self.contentOffset.y + self.bounds.size.height + 30;
            
            [UIView animateWithDuration:0.6
                                  delay:0
                 usingSpringWithDamping:1
                  initialSpringVelocity:0
                                options:UIViewAnimationOptionLayoutSubviews
                             animations:^{
                                 
                                 if (![tappedView isEqual:view])
                                 {
                                     view.frame = frame;
                                 }
                                 else
                                 {
                                     view.frame = bigFrame;
                                 }
                                 
                             }
                             completion:nil];
        }];
        
        [self.delegate verticalCarousel:self didSelectItemAtIndex:index];
        
        CDMVCInitialFrames = temp.copy;
    }
    
    tappedView.selected = !tappedView.selected;
}

- (void)animateShadow:(CDMVerticalCarouselView *)view
         withMaxAlpha:(float)maxAlpha
            andBounds:(CGRect)bounds
{
    static float CDMVCNormalAlpha;
    static CGRect CDMVCNormalRect;
    
    CAMediaTimingFunction *timing = [CAMediaTimingFunction functionWithControlPoints:0.36 :1 :0.35 :1];
    
    if (!view.selected)
    {
        CDMVCNormalAlpha = view.layer.shadowOpacity;
        CDMVCNormalRect = view.bounds;
        
        // animate to alpha 0.15
        CABasicAnimation *theAnimation =
        [CABasicAnimation animationWithKeyPath:@"shadowOpacity"];
        
        theAnimation.timingFunction = timing;
        theAnimation.duration = 0.6;
        theAnimation.fromValue = @(view.layer.shadowOpacity);
        theAnimation.toValue = @(maxAlpha);
        view.layer.shadowOpacity = maxAlpha;
        [view.layer addAnimation:theAnimation forKey:@"shadowOpacity"];
        
        CABasicAnimation *theBoundsAnimation =
        [CABasicAnimation animationWithKeyPath:@"shadowPath"];
        
        theBoundsAnimation.timingFunction = timing;
        theBoundsAnimation.duration = 0.6;
        theBoundsAnimation.fromValue = (id) [UIBezierPath bezierPathWithRect:view.bounds].CGPath;
        theBoundsAnimation.toValue = (id) [UIBezierPath bezierPathWithRect:bounds].CGPath;
        view.layer.shadowPath = [UIBezierPath bezierPathWithRect:bounds].CGPath;
        
        [view.layer addAnimation:theBoundsAnimation forKey:@"shadowPath"];
    }
    else
    {
        // animate to normal
        CABasicAnimation *theAnimation =
        [CABasicAnimation animationWithKeyPath:@"shadowOpacity"];
        
        theAnimation.timingFunction = timing;
        theAnimation.duration = 0.6;
        theAnimation.fromValue = @(maxAlpha);
        theAnimation.toValue = @(CDMVCNormalAlpha);
        view.layer.shadowOpacity = CDMVCNormalAlpha;
        [view.layer addAnimation:theAnimation forKey:@"shadowOpacity"];
        
        CABasicAnimation *theBoundsAnimation =
        [CABasicAnimation animationWithKeyPath:@"shadowPath"];
        
        theBoundsAnimation.timingFunction = timing;
        theBoundsAnimation.timeOffset = 0;
        theBoundsAnimation.duration = 0.6;
        theBoundsAnimation.fromValue =
        (id) [UIBezierPath bezierPathWithRect:bounds].CGPath;
        theBoundsAnimation.toValue =
        (id) [UIBezierPath bezierPathWithRect:CDMVCNormalRect].CGPath;
        view.layer.shadowPath =
        [UIBezierPath bezierPathWithRect:CDMVCNormalRect].CGPath;
        
        [view.layer addAnimation:theBoundsAnimation forKey:@"shadowPath"];
    }
}

@end
