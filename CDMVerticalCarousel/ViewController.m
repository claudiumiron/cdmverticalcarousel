//
//  ViewController.m
//  CDMVerticalCarousel
//
//  Created by Claudiu Miron on 15/12/15.
//  Copyright © 2015 Claudiu Miron. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to
//  deal in the Software without restriction, including without limitation the
//  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
//  sell copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
//  IN THE SOFTWARE.

#import "ViewController.h"

#import "CDMVerticalCarousel.h"

#import "VerticalCarouselView.h"

@interface ViewController () <CDMVerticalCarouselDataSource, CDMVerticalCarouselDelegate>

@property (weak, nonatomic) IBOutlet CDMVerticalCarousel *verticalCarousel;
@property NSArray *items;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Configure the carousel
    self.verticalCarousel.itemSpacing = 60;
    self.verticalCarousel.paddingInsets = UIEdgeInsetsMake(15, 6, -100, 6);
    self.verticalCarousel.falsePerspectiveMinimumWidthProportion = 0.97;
    self.verticalCarousel.heightProportion = 503.0 / 355.0;
    self.verticalCarousel.itemShadows = YES;
    
    // We have to reload if we set/change properties, as we've done above
    // Will change in future release
    [self.verticalCarousel reloadData];
}

#pragma mark - CDMVerticalCarouselDataSource

- (NSUInteger)numberOfItemsForVerticalCarousel:(CDMVerticalCarousel *)verticalCarousel
{
    return 30;
}

- (UIView *)verticalCarousel:(CDMVerticalCarousel *)verticalCarousel
                viewForIndex:(NSUInteger)index
{
    VerticalCarouselView *view = (VerticalCarouselView *) [verticalCarousel dequeueReusableView];
    
    if (!view)
    {
        view = [[VerticalCarouselView alloc] initWithFrame:CGRectZero];
        view.backgroundColor = [UIColor lightGrayColor];
    }
    
    view.label.text = [NSString stringWithFormat:@"%lu", (unsigned long) index];
    
    return view;
}

#pragma mark - CDMVerticalCarouselDelegate

- (void)verticalCarousel:(CDMVerticalCarousel *)verticalCarousel
    didSelectItemAtIndex:(NSUInteger)index
{
    NSLog(@"did select %lu", (unsigned long)index);
}

- (void)verticalCarousel:(CDMVerticalCarousel *)verticalCarousel
  didDeselectItemAtIndex:(NSUInteger)index
{
    NSLog(@"did deselect %lu", (unsigned long)index);
}

@end
