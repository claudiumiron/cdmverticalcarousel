//
//  main.m
//  CDMVerticalCarousel
//
//  Created by Claudiu Miron on 15/12/15.
//  Copyright © 2015 Claudiu Miron. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
